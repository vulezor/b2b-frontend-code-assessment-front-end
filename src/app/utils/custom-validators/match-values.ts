import { AbstractControl, ValidatorFn } from '@angular/forms';

export function valueMatchValidator(
  controlName: string,
  errorKey: string
): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const valueToMatch = control.parent?.get(controlName)?.value;

    if (control.value !== valueToMatch) {
      return { [errorKey]: true };
    }

    return null;
  };
}
