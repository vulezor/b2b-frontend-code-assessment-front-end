import { MatSnackBarConfig } from '@angular/material/snack-bar';

const commonOptions: MatSnackBarConfig<any> = {
  horizontalPosition: 'end',
  duration: 3000,
  verticalPosition: 'top',
};

export const successSnackbarOption: MatSnackBarConfig<any> = {
  ...commonOptions,
  panelClass: 'my-success-snackbar',
};

export const warningSnackbarOption: MatSnackBarConfig<any> = {
  ...commonOptions,
  panelClass: 'my-warning-snackbar',
};
