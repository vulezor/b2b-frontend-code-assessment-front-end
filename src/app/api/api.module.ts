import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IEnvironment, ENVIRONMENT } from './tokens/environment.token';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [],
    imports: [CommonModule, HttpClientModule],
    providers: [ApiService]
})
export class ApiModule {
    constructor(
        @Optional()
        @SkipSelf()
        parentModule: ApiModule
    ) {
        if (parentModule) {
            throw new Error('Api Module is already loaded. Import in the Core Module only');
        }
    }

    static forRoot(environment: IEnvironment): ModuleWithProviders<ApiModule> {
        return {
            ngModule: ApiModule,
            providers: [
                {
                    provide: ENVIRONMENT,
                    useValue: environment
                },
                ApiService
            ]
        };
    }
}
