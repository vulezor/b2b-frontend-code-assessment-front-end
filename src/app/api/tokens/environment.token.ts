import { InjectionToken } from '@angular/core';

export interface IEnvironment {
  environment: {
    production: boolean;
    baseApiUrl: string;
  };
}

export const ENVIRONMENT = new InjectionToken<IEnvironment>('IEnvironment');
