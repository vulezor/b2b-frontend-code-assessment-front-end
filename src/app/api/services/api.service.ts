import { Inject, Injectable } from '@angular/core';
import { ENVIRONMENT } from '../tokens/environment.token';
import { IEnvironment } from '../tokens/environment.token';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {
    constructor(@Inject(ENVIRONMENT) private env: IEnvironment, private http: HttpClient) {}

    post<T>(path: string, body: unknown, header = { 'content-type': 'application/json' }): Observable<any> {
        return this.http.post<T>(`${this.env.environment.baseApiUrl}${path}`, body, {
            headers: header
        });
    }

    get<T, P>(path: string, params?: P): Observable<T> {
        let queryParams = params ? { params: new HttpParams({ fromObject: params }) } : {};

        return this.http.get<T>(`${this.env.environment.baseApiUrl}${path}`, queryParams);
    }

    put<T>(path: string, body: unknown, header = { 'content-type': 'application/json' }) {
        return this.http.put<T>(`${this.env.environment.baseApiUrl}${path}`, body, {
            headers: header
        });
    }
}
