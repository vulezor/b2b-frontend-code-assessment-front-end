import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './core/guards/auth-guard.service';
import { LoginGuardService } from './core/guards/login-guard.service';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./public/public.module').then((m) => m.PublicModule),
        canActivate: [LoginGuardService]
    },
    {
        path: 'entry',
        loadChildren: () => import('./jobad/jobad.module').then((m) => m.JobadModule),
        canActivate: [AuthGuardService]
    },
    {
        path: '**',
        redirectTo: '/',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
