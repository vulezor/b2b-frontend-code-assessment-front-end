import { Pipe, PipeTransform } from '@angular/core';
import { STATUS } from './status';

@Pipe({
    name: 'stateTextChanger'
})
export class StateTextChangerPipe implements PipeTransform {
    transform(value: unknown): string | undefined {
        switch (value) {
            case STATUS.draft:
                return 'Publish';
            case STATUS.published:
                return 'Archive';
            default:
                return;
        }
    }
}
