export const STATUS = {
    draft: 'draft',
    published: 'published',
    archived: 'archived'
};
