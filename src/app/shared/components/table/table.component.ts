import {
    AfterContentInit,
    ChangeDetectionStrategy,
    Component,
    ContentChild,
    Input,
    OnInit,
    TemplateRef
} from '@angular/core';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit, AfterContentInit {
    @ContentChild('actionColumn')
    actionColumn!: TemplateRef<any> | null;

    displayedColumns!: string[];

    @Input()
    tableConfig!: Array<{ label: string; key: string }>;

    @Input()
    dataSource: Array<any> | null = [];

    ngOnInit() {}

    ngAfterContentInit(): void {
        this.tableConfig = this.actionColumn
            ? [...this.tableConfig, { label: 'Action', key: 'action' }]
            : this.tableConfig;
        this.displayedColumns = this.tableConfig.map((conf) => conf.key);
    }
}
