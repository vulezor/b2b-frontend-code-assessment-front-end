import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { PaginationMeta } from '../../../types/paginate-response.type';

@Component({
    selector: 'app-paginator',
    templateUrl: './paginator.component.html',
    styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnChanges {
    length = 0;
    pageSize = 0;
    pageIndex = 0;
    pageSizeOptions = [5, 10, 25];

    hidePageSize = false;
    showPageSizeOptions = true;
    showFirstLastButtons = true;
    disabled = false;

    pageEvent!: PageEvent;

    @Input()
    paginationMetaData!: PaginationMeta | null;

    @Output()
    paginationChanged: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

    handlePageEvent(e: PageEvent) {
        this.disabled = true;
        this.paginationChanged.emit(e);
    }

    setPageSizeOptions(setPageSizeOptionsInput: string) {
        if (setPageSizeOptionsInput) {
            this.pageSizeOptions = setPageSizeOptionsInput.split(',').map((str) => +str);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.disabled = false;

        const paginationMetaData = changes['paginationMetaData'];
        if (paginationMetaData.currentValue) {
            this.length = paginationMetaData.currentValue?.totalItems;
            this.pageIndex = paginationMetaData.currentValue?.currentPage - 1;
        }
    }
}
