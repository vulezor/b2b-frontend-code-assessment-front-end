import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class AuthService {
    private loggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    isUserLoggedIn(): Observable<boolean> {
        return this.loggedIn$.asObservable();
    }

    changeLoggedInStatus(value: boolean) {
        this.loggedIn$.next(value);
    }
}
