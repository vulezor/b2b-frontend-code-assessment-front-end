import { Injectable, Inject } from '@angular/core';
import { HttpRequest, HttpHandler, HttpErrorResponse, HttpInterceptor, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs/internal/observable/of';
import { LocalStorageService } from './local-storage.service';
import { ACCESS_TOKEN } from '../constants/constants';
import { AuthService } from './auth.service';
import { DOCUMENT } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
    constructor(
        private readonly localStorageService: LocalStorageService,
        private readonly router: Router,
        private readonly authService: AuthService,
        @Inject(DOCUMENT) private document: Document
    ) {}

    private setHeaders(request: HttpRequest<any>, accessToken: string): HttpRequest<any> {
        return (request = request.clone({
            setHeaders: {
                accept: 'application/json',
                Authorization: `Bearer ${accessToken}`
            }
        }));
    }

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> | any {
        const accessToken = this.localStorageService.getItem(ACCESS_TOKEN);
        if (request.url.includes('/assets/') || !accessToken) {
            return next.handle(request);
        }

        return next.handle(this.setHeaders(request, accessToken)).pipe(
            catchError((error: HttpErrorResponse): Observable<HttpEvent<any>> | Observable<any> => {
                switch ((error as HttpErrorResponse).status) {
                    case 401:
                        this.navigateToLoginAndClearTokens();
                        return throwError(error);
                    default:
                        return throwError(() => error);
                }
            })
        );
    }

    private navigateToLoginAndClearTokens(): void {
        this.authService.changeLoggedInStatus(false);
        this.localStorageService.removeItem(ACCESS_TOKEN);
        this.router.navigateByUrl(this.document.location.origin);
    }
}
