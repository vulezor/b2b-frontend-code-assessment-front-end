import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { ApiService } from '@api/services';
import { AuthService } from '../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class LoginGuardService implements CanActivate {
    constructor(
        private readonly apiService: ApiService,
        private readonly authService: AuthService,
        private router: Router
    ) {}

    public canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.isUserLoggedIn().pipe(
            switchMap((isLoggedIn: boolean) => {
                if (isLoggedIn) {
                    return of(false);
                }
                return this.apiService.get('/check_user_status').pipe(
                    map(() => {
                        this.router.navigate(['/entry']);
                        this.authService.changeLoggedInStatus(true);
                        return false;
                    }),
                    catchError((error: HttpErrorResponse) => {
                        return of(true);
                    })
                );
            })
        );
    }
}
