import { Inject, Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { map, switchMap, catchError } from 'rxjs/operators';
import { ApiService } from '@api/services';
import { AuthService } from '../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(
        private readonly apiService: ApiService,
        private readonly authService: AuthService,
        @Inject(DOCUMENT) private document: Document,
        private router: Router
    ) {}

    public canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.isUserLoggedIn().pipe(
            switchMap((isLoggedIn: boolean) => {
                if (isLoggedIn) {
                    return of(isLoggedIn);
                }
                return this.apiService.get('/check_user_status').pipe(
                    map(() => {
                        this.authService.changeLoggedInStatus(true);
                        return true;
                    }),
                    catchError((error: HttpErrorResponse) => {
                        this.router.navigateByUrl(this.document.location.origin);
                        return of(false);
                    })
                );
            })
        );
    }
}
