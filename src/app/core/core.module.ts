import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '@env';

import { ApiModule } from '../api/api.module';
import { LocalStorageService } from './services/local-storage.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { LoginGuardService } from './guards/login-guard.service';

@NgModule({
    declarations: [],
    imports: [ApiModule.forRoot({ environment }), CommonModule],
    exports: [],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorService,
            multi: true
        }
    ]
})
export class CoreModule {
    constructor(
        @Optional()
        @SkipSelf()
        parentModule: CoreModule
    ) {
        if (parentModule) {
            throw new Error('Core Module is already loaded. Import in the App Module only');
        }
    }

    static forRoot(): ModuleWithProviders<any> {
        return {
            ngModule: CoreModule,
            providers: [LocalStorageService, AuthService, AuthGuardService, LoginGuardService]
        };
    }
}
