import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { PublicRoutingModule } from './public-route.module';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [LoginComponent, RegisterComponent],
    imports: [CommonModule, PublicRoutingModule, SharedModule.forRoot()]
})
export class PublicModule {}
