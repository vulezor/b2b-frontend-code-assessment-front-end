import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from '@api/services';
import { TokenInterface } from '@core/interfaces';
import { AuthService, LocalStorageService } from '@core/services';
import { warningSnackbarOption } from '../../utils/snackbar-options/snackbar-options';
import { switchMap, tap } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
    form = this.formBuilder.group({
        email: ['', [Validators.email, Validators.required]],
        password: ['', [Validators.required]]
    });

    private destroyRef = inject(DestroyRef);

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly apiService: ApiService,
        private readonly localStorageService: LocalStorageService,
        private readonly authService: AuthService,
        private readonly _snackBar: MatSnackBar,
        private readonly router: Router
    ) {}

    login(): void {
        if (this.form.valid) {
            this.apiService
                .post<TokenInterface>('/login', this.form.value)
                .pipe(
                    takeUntilDestroyed(this.destroyRef),
                    tap((data: TokenInterface) => {
                        this.authService.changeLoggedInStatus(true);
                        this.localStorageService.setItem('accessToken', data.accessToken);
                    }),
                    switchMap(() => this.authService.isUserLoggedIn())
                )
                .subscribe({
                    next: (isLoggedin: boolean) => {
                        if (isLoggedin) {
                            this.router.navigate(['/entry']);
                        }
                    },
                    error: (error: HttpErrorResponse) => {
                        this._snackBar.open(error.error.message, 'close', warningSnackbarOption);
                    }
                });
        }
    }
}
