import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '@api/services';
import { valueMatchValidator } from '../../utils/custom-validators/match-values';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { switchMap, tap } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { warningSnackbarOption, successSnackbarOption } from '../../utils/snackbar-options/snackbar-options';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent {
    form = this.formBuilder.group({
        email: ['', [Validators.email, Validators.required]],
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required, valueMatchValidator('password', 'passwordMismatch')]]
    });

    private destroyRef = inject(DestroyRef);

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly apiService: ApiService,
        private readonly router: Router,
        private _snackBar: MatSnackBar
    ) {}

    /**
     * @description Register user method
     */
    register(): void {
        if (!this.form.valid) {
            return;
        }
        const payload = { ...this.form.value };
        this.form.reset();
        this.apiService
            .post('/users/create', payload)
            .pipe(
                takeUntilDestroyed(this.destroyRef),
                switchMap((data) => {
                    const snackBar = this._snackBar.open(
                        'You have successfully registered',
                        'close',
                        successSnackbarOption
                    );

                    return snackBar.afterDismissed();
                })
            )
            .subscribe({
                next: () => {
                    this.router.navigate(['/login']);
                },
                error: (error: HttpErrorResponse) => {
                    this._snackBar.open(error.error.message, 'close', warningSnackbarOption);
                }
            });
    }
}
