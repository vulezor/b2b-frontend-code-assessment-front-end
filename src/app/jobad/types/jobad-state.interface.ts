import { JobAd } from '@core/interfaces';
import { PaginationMeta } from '../../types/paginate-response.type';
import { FiltersInterface } from './filters.interface';

export interface JobAdStateInterface {
    isLoading: boolean;
    jobAds: JobAd[];
    paginationMeta: PaginationMeta | null;
    filters: FiltersInterface;
    error: string;
}
