import { JobAdStatus } from '@core/interfaces';

export interface FiltersInterface {
    page?: number;
    limit?: number;
    job_ad_status?: JobAdStatus;
    title?: string;
    order_direction?: 'ASC' | 'DESC';
    order_by?: OrderByValues;
}

export type FilterKeyValues = 'page' | 'title' | 'job_ad_status' | 'limit' | 'order_direction' | 'order_by';

export type OrderByValues = 'id' | 'title' | 'job_ad_status' | 'description';
