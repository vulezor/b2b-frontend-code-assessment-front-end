import { JobAd } from '@core/interfaces';
import { createAction, props } from '@ngrx/store';
import { PaginationMeta } from '../../types/paginate-response.type';
import {
    ADD_JOB_ADD,
    ADD_JOB_ADD_FAILURE,
    ADD_JOB_ADD_SUCCESS,
    GET_JOB_ADDS,
    GET_JOB_ADDS_FAILURE,
    GET_JOB_ADDS_SUCCESS,
    UPDATE_FILTERS,
    UPDATE_JOB_ADD,
    UPDATE_JOB_ADD_FAILURE,
    UPDATE_JOB_ADD_SUCCESS
} from './constants/job-ad-action-names';
import { FiltersInterface } from '../types/filters.interface';

/**Get paginate job ad list items actions */
export const getJobAds = createAction(GET_JOB_ADDS, props<{ queryParams: FiltersInterface }>());
export const getJobAdsSuccess = createAction(
    GET_JOB_ADDS_SUCCESS,
    props<{ jobAds: JobAd[]; paginationMeta: PaginationMeta }>()
);
export const getJobAdsFailure = createAction(GET_JOB_ADDS_FAILURE, props<{ error: string }>());

/**Add job ad info actions */
export const addJobAd = createAction(ADD_JOB_ADD, props<{ jobAd: JobAd }>());
export const addJobAdSuccess = createAction(ADD_JOB_ADD_SUCCESS);
export const addJobAdFailure = createAction(ADD_JOB_ADD_FAILURE, props<{ error: string }>());

/**Patch job ad info actions */
export const updateJobAd = createAction(UPDATE_JOB_ADD, props<{ jobAd: JobAd }>());
export const updateJobAdSuccess = createAction(UPDATE_JOB_ADD_SUCCESS, props<{ jobAd: JobAd }>());
export const updateJobAdFailure = createAction(UPDATE_JOB_ADD_FAILURE, props<{ error: string }>());

/**Filter actions */
export const updateFilersAction = createAction(UPDATE_FILTERS, props<{ filters: FiltersInterface }>());
