import { Injectable } from '@angular/core';
import { JobAd } from '@core/interfaces';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, delay, map, mergeMap, of, switchMap, tap } from 'rxjs';
import { PaginateResponse } from '../../types/paginate-response.type';
import { JobAdService } from '../services/jobad.service';
import * as JobAdActions from './actions';
import { successSnackbarOption } from 'src/app/utils/snackbar-options/snackbar-options';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class JobAdEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly jobAdService: JobAdService,
        private _snackBar: MatSnackBar,
        private router: Router
    ) {}

    createJobAds$ = createEffect(() =>
        this.actions$.pipe(
            ofType(JobAdActions.addJobAd),
            mergeMap((data) => {
                return this.jobAdService.createJobAd(data.jobAd).pipe(
                    switchMap(() => {
                        const snackBar = this._snackBar.open(
                            'You have succesfuly added job ad',
                            'close',
                            successSnackbarOption
                        );
                        return snackBar.afterDismissed().pipe(
                            tap(() => {
                                this.router.navigate(['/entry']);
                            })
                        );
                    }),
                    map(() => JobAdActions.addJobAdSuccess()),
                    catchError((error) => of(JobAdActions.addJobAdFailure({ error: error.message })))
                );
            })
        )
    );

    updateJobAds$ = createEffect(() =>
        this.actions$.pipe(
            ofType(JobAdActions.updateJobAd),
            mergeMap((data) => {
                return this.jobAdService.updateJobAd(data.jobAd).pipe(
                    map((jobAd: JobAd) => JobAdActions.updateJobAdSuccess({ jobAd })),
                    catchError((error) => of(JobAdActions.updateJobAdFailure({ error: error.message })))
                );
            })
        )
    );

    getJobAds$ = createEffect(() =>
        this.actions$.pipe(
            delay(500),
            ofType(JobAdActions.getJobAds),
            mergeMap((data) => {
                return this.jobAdService.getJobAds(data.queryParams).pipe(
                    map((jobAds: PaginateResponse<JobAd[]>) =>
                        JobAdActions.getJobAdsSuccess({ jobAds: jobAds.items, paginationMeta: jobAds.meta })
                    ),
                    catchError((error) => of(JobAdActions.getJobAdsFailure({ error: error.message })))
                );
            })
        )
    );
}
