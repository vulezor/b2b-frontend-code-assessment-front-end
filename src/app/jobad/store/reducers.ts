import { createReducer, on } from '@ngrx/store';
import { JobAdStateInterface } from '../types/jobad-state.interface';
import * as JobAdActions from './actions';
import { JobAd } from '@core/interfaces';
import { FiltersInterface } from '../types/filters.interface';
export const initialState: JobAdStateInterface = {
    isLoading: false,
    paginationMeta: null,
    jobAds: [],
    filters: {},
    error: ''
};

export const reducers = createReducer(
    initialState,
    /** Job actions */
    on(JobAdActions.getJobAds, (state, action) => ({
        ...state,
        isLoading: true
    })),
    on(JobAdActions.getJobAdsSuccess, (state, action) => ({
        ...state,
        isLoading: false,
        jobAds: [...action.jobAds],
        paginationMeta: { ...action.paginationMeta }
    })),
    on(JobAdActions.getJobAdsFailure, (state, action) => {
        return {
            ...state,
            isLoading: true,
            error: action.error
        };
    }),
    /** Add job ad  */
    on(JobAdActions.addJobAd, (state) => ({
        ...state,
        isLoading: true
    })),
    on(JobAdActions.addJobAdSuccess, (state) => ({
        ...state,
        isLoading: false
    })),
    on(JobAdActions.addJobAdFailure, (state, action) => ({
        ...state,
        isLoading: true,
        error: action.error
    })),
    /** Update job ad  */
    on(JobAdActions.updateJobAd, (state) => ({
        ...state,
        isLoading: true
    })),
    on(JobAdActions.updateJobAdSuccess, (state, action) => ({
        ...state,
        isLoading: true,
        jobAds: updateOldJobAdState(state, action)
    })),
    on(JobAdActions.updateJobAdFailure, (state, action) => ({
        ...state,
        isLoading: true,
        error: action.error
    })),
    /** Filter  */
    on(JobAdActions.updateFilersAction, (state, action) => {
        return {
            ...state,
            filters: action.filters
        };
    })
);

function updateOldJobAdState(state: JobAdStateInterface, action: { jobAd: JobAd }): JobAd[] {
    const jobAds = deepCopy<JobAd[]>(state.jobAds);
    const index = jobAds.findIndex((jobAd: JobAd) => jobAd.id === action.jobAd.id);
    if (index !== -1) {
        jobAds[index] = { ...action.jobAd };
    }
    return [...jobAds];
}

function deepCopy<T>(value: T): T {
    return JSON.parse(JSON.stringify(value));
}
