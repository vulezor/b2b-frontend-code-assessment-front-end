import { createSelector } from '@ngrx/store';
import { AppStateInterface } from '../../types/app-state.interface';

export const selectFeature = (state: AppStateInterface) => state.jobad;

export const isLoadingSelector = createSelector(selectFeature, (state) => state.isLoading);

export const jobAdsSelector = createSelector(selectFeature, (state) => state.jobAds);

export const jobAdResponseErrorSelector = createSelector(selectFeature, (state) => state.error);

export const paginationMetaDataSelector = createSelector(selectFeature, (state) => state.paginationMeta);

export const filterSelector = createSelector(selectFeature, (state) => state.filters);
