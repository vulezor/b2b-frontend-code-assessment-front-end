const JOB_ADD_SPACENAME = '[JOBAD]';

export const GET_JOB_ADDS = `${JOB_ADD_SPACENAME} Get job ads`;
export const GET_JOB_ADDS_SUCCESS = `${JOB_ADD_SPACENAME} Get job ads success`;
export const GET_JOB_ADDS_FAILURE = `${JOB_ADD_SPACENAME} Get job ads failure`;

export const ADD_JOB_ADD = `${JOB_ADD_SPACENAME} Add job ad`;
export const ADD_JOB_ADD_SUCCESS = `${JOB_ADD_SPACENAME} Add job ad success`;
export const ADD_JOB_ADD_FAILURE = `${JOB_ADD_SPACENAME} Add job ad failure`;

export const UPDATE_JOB_ADD = `${JOB_ADD_SPACENAME} Update job ad`;
export const UPDATE_JOB_ADD_SUCCESS = `${JOB_ADD_SPACENAME} Update job ad success`;
export const UPDATE_JOB_ADD_FAILURE = `${JOB_ADD_SPACENAME} Update job ad failure`;

export const UPDATE_FILTERS = `${JOB_ADD_SPACENAME} Update filters`;
