import { ChangeDetectionStrategy, Component, DestroyRef, OnInit, inject } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { JobAd, JobAdStatus } from '@core/interfaces';
import { select, Store } from '@ngrx/store';
import { Observable, debounceTime } from 'rxjs';
import { AppStateInterface } from '../../types/app-state.interface';
import { PaginationMeta } from '../../types/paginate-response.type';
import * as JobAdActions from '../store/actions';
import {
    filterSelector,
    isLoadingSelector,
    jobAdResponseErrorSelector,
    jobAdsSelector,
    paginationMetaDataSelector
} from '../store/selectors';

import { STATUS } from '../../shared/pipes/status';
import { FormBuilder } from '@angular/forms';
import { FiltersInterface } from '../types/filters.interface';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { trigger, transition, animate, style, state } from '@angular/animations';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('welcomeAnimation', [
            state(
                'visible',
                style({
                    opacity: 1
                })
            ),

            state(
                'void, hidden',
                style({
                    marginTop: '100px',
                    opacity: 0
                })
            ),
            transition('*=>visible', animate('500ms ease-out')),
            transition('*=>void, *=>hidden', animate('250ms'))
        ])
    ]
})
export class ListComponent implements OnInit {
    isVisible = false;
    tableConfig = [
        { label: 'Id', key: 'id' },
        { label: 'Title', key: 'title' },
        { label: 'Description', key: 'description' },
        { label: 'Status', key: 'jobAdStatus' }
    ];
    isLoading$: Observable<boolean>;

    jobAds$: Observable<JobAd[]>;

    jobAdsResponseError$: Observable<string>;

    paginationMetaData$: Observable<PaginationMeta | null>;

    filterData$: Observable<FiltersInterface>;

    filterForm = this.formBuilder.group({
        title: [''],
        jobAdStatus: ['']
    });

    currentFilters: FiltersInterface = {};

    get titleFormControl() {
        return this.filterForm.get('title');
    }

    get statusFormControl() {
        return this.filterForm.get('jobAdStatus');
    }

    private destroyRef = inject(DestroyRef);

    constructor(
        private readonly store: Store<AppStateInterface>,
        private readonly router: Router,
        private readonly route: ActivatedRoute,
        private readonly formBuilder: FormBuilder
    ) {
        this.isLoading$ = store.pipe(select(isLoadingSelector));
        this.jobAds$ = store.pipe(select(jobAdsSelector));
        this.jobAdsResponseError$ = this.store.pipe(select(jobAdResponseErrorSelector));
        this.paginationMetaData$ = this.store.pipe(select(paginationMetaDataSelector));
        this.filterData$ = this.store.pipe(select(filterSelector));
    }

    ngOnInit(): void {
        this.toggle();
        this.setDefaultFilters();
        this.listenTitleFormControl();
        this.listenStatusFormControl();

        this.filterData$.pipe(takeUntilDestroyed(this.destroyRef)).subscribe((filters) => {
            this.currentFilters = { ...filters };
            this.navigateWithFilters(this.currentFilters);
            this.store.dispatch(JobAdActions.getJobAds({ queryParams: this.currentFilters }));
        });
    }

    /**
     * @description it returns page event with page size an page number and update filters
     * @param e PageEvent from the paginator
     */
    afterPaginationChange(e: PageEvent): void {
        const filters = this.mergeFilters({ limit: +e.pageSize, page: +e.pageIndex + 1 });
        this.store.dispatch(JobAdActions.updateFilersAction({ filters }));
    }

    /**
     * @patching selected job for status changes
     * @param jobAd JobAd
     */
    changeStatus(jobAd: JobAd) {
        const jobAdToUpdate = { ...jobAd };
        const statusNameToUpdate = this.checkWhichStatusNameToUpdate(jobAdToUpdate.jobAdStatus);
        jobAdToUpdate['jobAdStatus'] = statusNameToUpdate ? statusNameToUpdate : jobAdToUpdate.jobAdStatus;
        this.store.dispatch(JobAdActions.updateJobAd({ jobAd: jobAdToUpdate }));
    }

    createNewJobAd() {
        this.router.navigate(['./create'], { relativeTo: this.route });
    }

    editJobAd(id: number) {
        this.router.navigate([`./edit/${id}`], { relativeTo: this.route });
    }

    toggle() {
        this.isVisible = !this.isVisible;
    }

    private checkWhichStatusNameToUpdate(status: JobAdStatus): JobAdStatus | undefined {
        switch (status) {
            case STATUS.draft:
                return STATUS.published as JobAdStatus;
            case STATUS.published:
                return STATUS.archived as JobAdStatus;
            default:
                return;
        }
    }

    private listenTitleFormControl(): void {
        this.titleFormControl?.valueChanges
            ?.pipe(debounceTime(300), takeUntilDestroyed(this.destroyRef))
            .subscribe((titleTerm) => {
                if (titleTerm) {
                    this.store.dispatch(
                        JobAdActions.updateFilersAction({ filters: this.mergeFilters({ title: titleTerm }) })
                    );

                    return;
                }
                const { title, ...filters } = { ...this.currentFilters };
                this.store.dispatch(JobAdActions.updateFilersAction({ filters: { ...filters, page: 1 } }));
            });
    }

    /**
     * @description Listen on the form control title and integrate or delete property form filters
     */
    private listenStatusFormControl(): void {
        this.statusFormControl?.valueChanges.pipe(takeUntilDestroyed(this.destroyRef)).subscribe((status): void => {
            if (status) {
                this.store.dispatch(
                    JobAdActions.updateFilersAction({
                        filters: this.mergeFilters({ job_ad_status: status as JobAdStatus, page: 1 })
                    })
                );

                return;
            }

            const { job_ad_status, ...filters } = this.currentFilters;
            this.store.dispatch(JobAdActions.updateFilersAction({ filters }));
        });
    }

    /***
     * Set default filters after land on the page and update state of filters from query params
     */
    private setDefaultFilters() {
        const queryParams = this.prepareQueryParams();
        const filters = this.mergeFilters({
            ...queryParams,
            limit: queryParams.limit ? +queryParams.limit : 5,
            page: queryParams.page ? +queryParams.page : 1
        });

        this.updateFilterFormControls(queryParams);

        this.store.dispatch(JobAdActions.updateFilersAction({ filters }));
    }

    /**
     * @param filters
     * @returns FiltersInterface as merged filters
     */
    private mergeFilters(filters: FiltersInterface): FiltersInterface {
        return { ...this.currentFilters, ...filters };
    }

    /**
     * @description
     * @param params FiltersInterface
     */
    private navigateWithFilters(filters: FiltersInterface): void {
        this.router.navigate(['./'], {
            relativeTo: this.route,
            queryParams: filters
        });
    }

    private updateFilterFormControls(queryParams: FiltersInterface): void {
        if (queryParams['job_ad_status']) {
            this.statusFormControl?.patchValue(queryParams['job_ad_status']);
        }

        if (queryParams['title']) {
            this.titleFormControl?.patchValue(queryParams['title']);
        }
    }

    /**
     * @prepare object of filters from the query params
     * @returns FiltersInterface
     */
    private prepareQueryParams(): FiltersInterface {
        const queryParams: { [key: string]: string } = {};

        this.route.snapshot.queryParamMap.keys.forEach((key: any) => {
            queryParams[key] = this.route.snapshot.queryParamMap.get(key) as string;
        });
        return queryParams as FiltersInterface;
    }
}
