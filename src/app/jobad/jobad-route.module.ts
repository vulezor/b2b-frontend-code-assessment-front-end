import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list/list.component';
import { EntryComponent } from './entry/entry.component';
import { CreateEditJobAdComponent } from './create-edit-jobad/create-edit-jobad.component';
import { JobAdResolverService } from './services/job-ad-resolver.service';
import { NoFoundPageComponent } from './no-found-page/no-found-page.component';

const routes: Routes = [
    {
        path: '',
        component: EntryComponent,
        children: [
            {
                path: '',
                component: ListComponent
            },
            {
                path: 'not-found',
                component: NoFoundPageComponent
            },
            {
                path: 'create',
                component: CreateEditJobAdComponent
            },
            {
                path: 'edit/:id',
                component: CreateEditJobAdComponent,
                resolve: { jobAd: JobAdResolverService }
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class JobAdRoutingModule {}
