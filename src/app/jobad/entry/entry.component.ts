import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, LocalStorageService } from '@core/services';
import { ACCESS_TOKEN } from '../../core/constants/constants';

@Component({
    selector: 'app-entry',
    templateUrl: './entry.component.html',
    styleUrls: ['./entry.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntryComponent {
    constructor(
        private readonly localStorageService: LocalStorageService,
        private readonly authService: AuthService,
        private readonly router: Router
    ) {}

    logout() {
        this.authService.changeLoggedInStatus(false);
        this.localStorageService.removeItem(ACCESS_TOKEN);
        this.router.navigate(['/']);
    }
}
