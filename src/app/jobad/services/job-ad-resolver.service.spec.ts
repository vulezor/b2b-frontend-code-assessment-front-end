import { TestBed } from '@angular/core/testing';

import { JobAdResolverService } from './job-ad-resolver.service';

describe('JobAdReducerService', () => {
    let service: JobAdResolverService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(JobAdResolverService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
