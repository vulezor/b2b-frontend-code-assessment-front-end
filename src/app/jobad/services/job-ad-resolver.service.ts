import { Injectable } from '@angular/core';
import { JobAdService } from './jobad.service';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable, catchError, of, switchMap } from 'rxjs';
import { JobAd } from '@core/interfaces';
import { AppStateInterface } from '../../types/app-state.interface';
import { Store, select } from '@ngrx/store';
import { jobAdsSelector } from '../store/selectors';

@Injectable({
    providedIn: 'root'
})
export class JobAdResolverService implements Resolve<any> {
    private jobAds$: Observable<JobAd[]>;

    constructor(
        private readonly jobAdService: JobAdService,
        private readonly store: Store<AppStateInterface>,
        private readonly router: Router
    ) {
        this.jobAds$ = store.pipe(select(jobAdsSelector));
    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | undefined {
        let jobId: string | number | null = route.paramMap.get('id');
        jobId = jobId ? +jobId : jobId;
        return this.jobAds$.pipe(
            switchMap((jobAds) => {
                if (jobId) {
                    const jobAd = jobAds.find((job) => job.id === jobId);
                    if (jobAd) {
                        return of(jobAd);
                    }
                    return this.jobAdService.getJobAd(+jobId).pipe(
                        catchError((error) => {
                            this.router.navigate(['/entry/not-found']);
                            return of(error.message);
                        })
                    );
                }
                return of(false);
            })
        );
    }
}
