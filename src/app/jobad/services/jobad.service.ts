import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@api/services';
import { JobAd } from '@core/interfaces';
import { Observable } from 'rxjs';
import { PaginateResponse } from '../../types/paginate-response.type';

@Injectable()
export class JobAdService {
    constructor(private readonly apiService: ApiService) {}

    createJobAd(data: JobAd): Observable<JobAd> {
        return this.apiService.post<JobAd>(`/jobads/create`, data);
    }

    updateJobAd(data: JobAd): Observable<JobAd> {
        return this.apiService.put<JobAd>(`/jobads/${data.id}/update`, data);
    }

    getJobAd(id: number): Observable<JobAd> {
        return this.apiService.get<JobAd, null>(`/jobads/${id}/fetch_jobad`);
    }

    getJobAds(options?: any): Observable<PaginateResponse<JobAd[]>> {
        let queryParams = options || null;
        return this.apiService.get<PaginateResponse<JobAd[]>, typeof options>('/jobads/find_all', queryParams);
    }
}
