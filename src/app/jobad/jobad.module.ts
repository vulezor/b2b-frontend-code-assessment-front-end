import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { JobAdRoutingModule } from './jobad-route.module';
import { EntryComponent } from './entry/entry.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { JobAdService } from './services/jobad.service';
import { reducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { JobAdEffects } from './store/effects';
import { CreateEditJobAdComponent } from './create-edit-jobad/create-edit-jobad.component';
import { JobAdResolverService } from './services/job-ad-resolver.service';
import { NoFoundPageComponent } from './no-found-page/no-found-page.component';

@NgModule({
    declarations: [EntryComponent, ListComponent, CreateEditJobAdComponent, NoFoundPageComponent],
    imports: [
        CommonModule,
        JobAdRoutingModule,
        SharedModule.forRoot(),
        StoreModule.forFeature('jobad', reducers),
        EffectsModule.forFeature([JobAdEffects])
    ],
    providers: [JobAdService, JobAdResolverService]
})
export class JobadModule {}
