import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppStateInterface } from '../../types/app-state.interface';
import * as JobAdActions from '../store/actions';
import { JobAd } from '@core/interfaces';
import { STATUS } from '../../shared/pipes/status';

export interface Skill {
    name: string;
}

@Component({
    selector: 'app-create-edit-jobad',
    templateUrl: './create-edit-jobad.component.html',
    styleUrls: ['./create-edit-jobad.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('welcomeAnimation', [
            state(
                'visible',
                style({
                    opacity: 1
                })
            ),
            state(
                'void, hidden',
                style({
                    marginTop: '100px',
                    opacity: 0
                })
            ),
            transition('*=>visible', animate('500ms ease-out')),
            transition('*=>void, *=>hidden', animate('250ms'))
        ])
    ]
})
export class CreateEditJobAdComponent implements OnInit {
    isVisible = true;

    form = this.formBuilder.group({
        title: ['', Validators.required],
        description: ['', Validators.required],
        skills: this.formBuilder.array([]),
        jobAdStatus: ['draft']
    });

    get skills(): FormArray<any> {
        return this.form.get('skills') as FormArray;
    }

    isEdit = false;

    jobId!: number;

    isArchived = false;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly router: Router,
        private readonly route: ActivatedRoute,
        private readonly store: Store<AppStateInterface>
    ) {
        this.route.data.subscribe((response: any) => {
            if (response.jobAd) {
                this.isArchived = response.jobAd.jobAdStatus === STATUS.archived;
                this.jobId = response.jobAd.id;
                this.isEdit = true;
                for (let skill in response.jobAd.skills) {
                    this.addNewSkill();
                }
                this.form.setValue({
                    title: response.jobAd.title,
                    description: response.jobAd.description,
                    skills: response.jobAd.skills,
                    jobAdStatus: response.jobAd.jobAdStatus
                });
                if (this.isArchived) {
                    this.form.disable();
                }
            }
        });
    }

    ngOnInit(): void {
        if (!this.isEdit) {
            this.addNewSkill();
        }
    }

    goToList() {
        this.router.navigate(['/entry'], { relativeTo: this.route });
    }

    addNewSkill() {
        this.skills.push(new FormControl(''));
    }

    removeSkill(index: number) {
        this.skills.removeAt(index);
    }

    submit() {
        if (this.form.valid) {
            const action = !this.isEdit
                ? JobAdActions.addJobAd({ jobAd: { id: null, ...this.form.value } as JobAd })
                : JobAdActions.updateJobAd({
                      jobAd: { id: this.jobId, ...this.form.value } as JobAd
                  });
            this.store.dispatch(action);
        }
    }
}
