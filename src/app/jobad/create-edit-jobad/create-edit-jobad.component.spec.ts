import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditJobAdComponent } from './create-edit-jobad.component';

describe('CreateEditJobAdComponent', () => {
    let component: CreateEditJobAdComponent;
    let fixture: ComponentFixture<CreateEditJobAdComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CreateEditJobAdComponent]
        });
        fixture = TestBed.createComponent(CreateEditJobAdComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
