export type PaginateResponse<T> = {
    items: T;
    meta: PaginationMeta;
};

export type PaginationMeta = {
    totalItems: number;
    itemCount: number;
    itemsPerPage: number;
    totalPages: number;
    currentPage: number;
};
