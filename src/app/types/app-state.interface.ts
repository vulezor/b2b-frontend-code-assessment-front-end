import { JobAdStateInterface } from '../jobad/types/jobad-state.interface';

export interface AppStateInterface {
    jobad: JobAdStateInterface;
}
