import { IEnvironment } from './environment.interface';

export const environment: IEnvironment = {
    production: false,
    baseApiUrl: 'http://localhost:5200'
};
