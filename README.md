## Development environment successfully tested and develop

windows 10, Chrome

## Node version

Node v16.14.0

## Anguar version

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.0.0.

## Requrements and dependencies and note

in order for the frontend app to work there is a necessity for you to set up a backend related to this app which is part of this task of coding challenge.
The backend is built with nests and typeorm. Beare in mind this is my first involvement in this backend framework mainly i used php laravel but for this purpose, I take nest as you suggested as plus.

### Backend repository and setup

For the backend you will need to instal [Docker](https://www.docker.com/)

The backend related to this fronted app is on this url [b2b-frontend-code-assessment-backend-end](https://gitlab.com/vulezor/b2b-frontend-code-assessment-backend-end). Go there and clone project to your local.

Do npm install to install dependencies. I have used Node v16.14.0.

```bash
npm install
```

You will need to rename .env.example to .env and after you need to add some missing information in regards to database.For instance

```bash
MYSQL_DB_USER=txservices
MYSQL_DB_PASSWORD=secret
MYSQL_DB_ROOT_PASSWORD=root
MYSQL_DB_NAME=txservices_db
```

After you finished setup of environment variables for database you can start the project with siple command.

```bash
docker-compose up
```

If you follow this proces you backend app is ready an it listen on localhost:5200. if you add this url to browser you should see `Welcome. Your app works!`

Done. Now setup of frontend. :)

# Frontend Setup

The fronted app is on this url [b2b-frontend-code-assessment-front-end](https://gitlab.com/vulezor/b2b-frontend-code-assessment-front-end). Go there and clone project to your local.

Do npm install to install dependencies. I have used Node v16.14.0.

```bash
npm install
```

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`.

If you experienced any issue you can send me email on vulezor@gmail.com or you can call me on 0605051578. Cheers
